unit tedradky;

{$IFDEF VER2}{$DEFINE NEWFPC}{$ENDIF}
{$IFDEF VER3}{$DEFINE NEWFPC}{$ENDIF}
{$IFDEF NEWFPC}{$CALLING OLDFPCCALL}{$ENDIF}

{$I defines.inc}
interface
uses dos,vaznik;

{------------------------------Pro EditacniPole-----------------------------}
const ED_GRANULARITA = 32;
{Po jak velkych kusech bude alokovat pamet pro retezce. Mela by byt
nasobkem 16. Zvyseni muze praci mirne urychlit, ale zvysi se pametove naroky}

{PEdRadek je myslen jako nahrada typu AnsiString, ktery je pomaly a ve verzi
1.0.10 v nekterych pripadech (v potomcich objektu) blbne.}
type

Pfnatrb = ^Tfnatrb;
Tfnatrb = object
  pozice:longint;          {pozice v textu (od 1), od ktere to plati}
  extra:pstring;           {extra tagy jako treba obrazek, preskok, atd.}

  {atrb:TFAtrb;}             {vlastni soubor vlastnosti fontu}


  font:pointer;            {ukazatel na font ve formatu FN}
  barva:word;              {barva}
  pozadi:longint;
  podtrh:boolean;


  Constructor Init;
  Procedure Default;
  Function CopyTo(newpos:longint):PFnatrb;
  {POZOR - nekopiruje extratagy}
  Function CopyEx:PFNatrb;
  {Kopiruje vcetne extratagu}
  Destructor Done;
  end;


PEdRadek = ^TEdRadek;
TEdRadek = object
       p:pchar;                  {buffer na znaky radku}
       pp:longint;               {velikost bufferu}
       spp:longint;              {skutecne zaplneny pocet bajtu}
       Constructor Init;
       Procedure Vloz(s:pchar;sd,poz:longint);virtual;
       Procedure Vloz(s:pchar;poz:longint);virtual;

       Procedure VlozS(s:string;poz:longint);virtual;
       Procedure VlozKus(s:pchar;poz:longint;s_zac,s_del:longint);virtual;
       Procedure Vyjmi(poz,l:longint);virtual;
       Procedure Zamena(s:pchar);virtual;
       Procedure ZamenaS(s:string);virtual;
       Function Delka:longint;virtual;
       Function Znak(n:longint):word;virtual;
       Function Najdi(s:string;poz:longint):longint;virtual;
       Procedure InternalVratCast(var poz,delk:longint;buffer:pchar);virtual;
       Function VratPchar(poz,delk:longint):pchar;virtual;
       Function VratString(poz,delk:longint):string;virtual;
       Function VS:string;         {vrati string}
       Function VP:pchar;          {vrati pchar}
       Function VratUsek(poz,del:longint):PEdRadek;virtual;
       Procedure Rozdel(var nv:TEdRadek;pozice,vypust:longint);virtual;
       {rozdeli retezec na dve casti. nedela zadnou realokaci ani presuny pameti}
       {pri tom muze ze zacatku druheho radku vypustit VYPUST znaku}
       Procedure Rozdel(var nv:TEdRadek;pozice:longint);virtual;
       Function ZacatekSlova(poz:longint):longint;
       Function KonecSlova(poz:longint):longint;
       Procedure Slovo(poz:longint;var zac,kon:longint);
       Destructor Done;virtual;
       end;

PItRadek = ^TItRadek;
TItRadek = object(TEdRadek)
       {Z duvodu podpory unicode zabira kazdy znak dva bajty. Lhostejno, je-li}
       {to skutecne unicode retezec nebo ne}
       {Vsechny parametry urcujici pozici nebo delku se zadavaji ve znacich,}
       {nikoliv v bytech}
       up:longint;               {je to (spp-1) div 2}
       aa:Pvaznik;               {plody jsou typu Pfnatrb}
       y1,y2:longint;            {Y-ova pozice na pomyslne strance (pocita se od 0)}
       so,su:longint;            {vyska textu nad linkou a pod linkou}
       bu:longint;               {vyska nejvyssiho obrazku}
                                 {(pocita se od zakladni linky)}

       yy:longint;               {cislo radku}
       gd:longint;               {delka radku v pixelech}
       smezerou:boolean;         {pri zalamovani - jestli je v retezci mezera, tak zalomim podle mezery. Jestli ne, zalomim natrvrdo}
                                 {pri nacitani se sem, uklada, jestli je radek uz ukoncen nebo zatim ne}
       crlf:boolean;             {je radek ukoncen natvrdo? DEFAULTNE TRUE}
       odz,doz:longint;          {prvni a posledni kompletne vypsane znaky}
       posunm,posunv:longint;
       lch:word;                 {ktery je prvni znak co nebyl vypsan?}

       Constructor Init;
       Constructor Init(s:string);
       Procedure PrvotniPrirazeni;
       Procedure UtvorZakladniUzel;
       Function Delka:longint;virtual;
       Function Vyska:longint;virtual;
       Function VratUsek(poz,del:longint):PItRadek;
       Function ZkopirujUsekVaznikuUzlu(poz,del:longint):PVaznik;
       Function Znak(n:longint):word;virtual;
       Procedure InternalVratCast(var poz,delk:longint;buffer:pchar);virtual;

       Function ZrusUzly(poz,l:longint):boolean; {zrusi vsechny uzly krome 1.}
       Function VratUzel(poz:longint):Pfnatrb;
       {vrati atributovy uzel platny pro specifikovanou pozici}
       Function VratVaznikUzlu(poz:longint):PUzel;virtual;
       {vraci odkaz na tento uzel}

       Function Najdi(s:string;poz:longint):longint;virtual;
       Procedure Vloz(s:pchar;sd,poz:longint);virtual;
       {Proceduru VlozS neni nutne deklarovat, protoze se pres virtualni
        metodu zavola nas Vloz}
       Procedure VlozWS(t:pchar;td,poz:longint;b:boolean);
       Procedure VlozWord(o:word;poz:longint);
       Procedure Vyjmi(poz,l:longint);virtual;
       Procedure VyjmiPosl;virtual;   {smaze posledni znak retezce}
       Procedure Smaz;virtual;        {smaze cely retezec}
       Procedure VlozKus(s:pchar;poz:longint;s_zac,s_del:longint);virtual;
       Procedure VlozKusIT(s:PItRadek;poz,s_zac,s_del:longint);
       Procedure Pripoj(s:PItRadek);
       Procedure UmistiUzel(u:PFNatrb;poz:longint);
       Procedure PrvniUzel;
       Procedure Rozdel(var nv:TItRadek;pozice:longint);virtual;
       Procedure Rozdel(var nv:TItRadek;pozice,vypust:longint);
       Procedure Obnov_BU;
       Function Copy:PItRadek;
       Destructor Done;virtual;
       end;

implementation
uses FNfont2,Lacrt,VenomGFX;
const ochrana_obrazku:boolean = false;


Constructor Tfnatrb.Init;
begin
extra:=nil;
podtrh:=false;
pozadi:=-1;
barva:=65535;
pozice:=1;
font:=nil;
end;

Procedure Tfnatrb.Default;
begin
font:=fn_default_fn;
barva:=FN_color;
pozadi:=FN_pozadi;
podtrh:=FN_podtrh;
pozice:=1;
end;

Function Tfnatrb.CopyTo(newpos:longint):PFnatrb;
{POZOR - nebude kopirovat extratagy}
var e:Pfnatrb;
begin
e:=New(PfnAtrb,Init);
e^.font:=font;
e^.barva:=barva;
e^.pozadi:=pozadi;
e^.podtrh:=podtrh;
e^.pozice:=newpos;
CopyTo:=e;
end;

Function Tfnatrb.CopyEx:PFNatrb;
{Zkopiruje vcetne extratagu}
var e:Pfnatrb;
begin
e:=CopyTo(pozice);
if extra<>nil then e^.extra:=NaPstring(extra^);
CopyEx:=e;
end;

Destructor Tfnatrb.Done;
var a,xr,yr,xp,yp,clr:longint;
    v:PVirtualWindow;
    n:boolean;
begin
if extra<>nil then                  {Je na tomto miste extratag?}
   begin
   if ochrana_obrazku=false then
      begin
      a:=1;
      repeat
         {n:=VytahniObrazek(extra^,a,v,xr,yr,xp,yp,clr);}n:=false;
         if N then begin Kill_VW(v^);Dispose(v);end;
      until n=false;
      end;
   FreeMem(extra);
   end;
end;

{$IFDEF FPC}
Function PLength(p:pchar):longint;assembler;
asm
xor eax,eax
mov esi,p
@znova:
cmp byte [esi],0
je @konec
inc esi
inc eax
jmp @znova
@konec:
end;

{$ELSE}
Function PLength(p:pchar):longint;
var q:pchar;
    l:longint;
begin
l:=0;
q:=p;
while q^<>#0 do begin inc(q);inc(l);end;
PLength:=l;
end;
{$ENDIF}

Procedure Kill_Pfnatrb(var p:Pointer);
var h:pfnatrb;
begin
h:=p;
Dispose(h,Done);
end;

Constructor TEdRadek.Init;
begin
spp:=1;
pp:=ED_GRANULARITA;
GetMem(p,pp);
p[0]:=#0;
end;

Function TEdRadek.Delka:longint;
begin
Delka:=spp-1;
end;

Function TEdRadek.Znak(n:longint):word;
begin
Znak:=word(p[n-1]);
end;


Procedure TEdRadek.InternalVratCast(var poz,delk:longint;buffer:pchar);
var i:longint;
begin
poz:=poz-1;
i:=delka;
if poz>=i then poz:=i-1;
if poz<0 then poz:=0;
if poz+delk-1>i then delk:=i-poz+1;
for i:=0 to delk-1 do buffer[i]:=p[i+poz];
end;

Function TEdRadek.VratPchar(poz,delk:longint):pchar;
var t:pchar;
    i:longint;
begin
i:=delk;
GetMem(t,delk+1);
InternalVratCast(poz,delk,t);
if delk<>i then ReAllocMem(t,delk+1);
t[delk]:=#0;
VratPChar:=t;
end;

Function TEdRadek.VratString(poz,delk:longint):string;
var t:string;
begin
InternalVratCast(poz,delk,@t[1]);
t[0]:=char(delk);
VratString:=t;
end;


Function TEdRadek.vs:string;
begin
vs:=VratString(1,delka);
end;

Function TEdRadek.vp:pchar;
begin
vp:=VratPChar(1,delka);
end;

Function TEdRadek.VratUsek(poz,del:longint):PEdRadek;
var v:PEdRadek;
begin
v:=New(PEdRadek,Init);
v^.VlozKus(p,1,poz,del);
VratUsek:=v;
end;

Function TEdRadek.Najdi(s:string;poz:longint):longint;
var a,b,c:longint;
    ne:boolean;
begin
b:=Length(s);
for a:=poz-1 to delka-b do
   begin
   ne:=false;
   for c:=1 to b do
      if p[a+c-1]<>s[c] then begin ne:=true;Break;end;
   if NE=false then Exit(a+1);
   end;
Najdi:=0;
end;

Procedure TEdRadek.Vloz(s:pchar;sd,poz:longint);
var np:Pchar;
    opp:longint;
begin
if poz<1 then poz:=1;
if poz>spp then poz:=spp;
dec(poz);              {pozici budu cislovat od 1, tak jako u typu string}
if sd=0 then Exit;      {diskutabilni. snad je to OK}
if spp+sd>pp then       {bude treba provest realokaci pameti}
   begin
   opp:=pp;
   pp:=((spp+sd) div ED_GRANULARITA+1)*ED_GRANULARITA;
   GetMem(np,pp);      {pripravim novy buffer}
   Move(p[0],np[0],poz);    {cast stareho pred vlozenim noveho}
   Move(s[0],np[poz],sd+1); {novy kus dam za stary a zkopiruju i #0}
   if spp-1>poz then
      Move(p[poz],np[poz+sd],spp-poz+1); {cast stareho, ktera ted bude}
                                             {za novym. Znovu kupiruju i s #0}
   inc(spp,sd);
   FreeMem(p,opp);
   p:=np;
   end
   else begin          {realokace pameti nebude treba}
   Move(p[poz],p[poz+sd],spp-poz); {posun textu vpravo od vlozeneho}
   Move(s[0],p[poz],sd);          {a vlozeni S}
   inc(spp,sd);
   end;
end;

Procedure TEdRadek.Vloz(s:pchar;poz:longint);
var i:longint;
begin
i:=PLength(s);
Vloz(s,i,poz);
end;

Procedure TEdRadek.VlozS(s:string;poz:longint);
var qq:pchar;
    i:longint;
begin
i:=Length(s);
s:=s+#0;
qq:=@s[1];
Vloz(qq,i,poz);
end;

Procedure TEdRadek.VlozKus(s:pchar;poz:longint;s_zac,s_del:longint);
var c:char;
begin
dec(s_zac);
inc(s,s_zac);
c:=s[s_del];
s[s_del]:=#0;
Vloz(s,s_del,poz);
s[s_del]:=c;
end;

Procedure TEdRadek.Vyjmi(poz,l:longint);
var t1,t2:Pchar;
    np:longint;
begin
if poz>spp-1 then exit;
if poz<1 then begin dec(l,-(poz-1));poz:=1;end;
{jako pocatecni pozice zadana 0 nebo zaporne cislo?}

if l<1 then exit;
{chceme smazat nulovy nebo zaporny pocet znaku?}

if poz+l>spp then l:=spp-poz;
dec(poz);
t1:=p;inc(t1,poz);
t2:=p;inc(t2,poz+l);
Move(t2^,t1^,spp-poz-1);
dec(spp,l);
np:=(spp div ED_GRANULARITA+1)*ED_GRANULARITA;
if np<>pp then ReAllocMem(p,np);  {P uz ale muze ukazovat jinam (obsah P se muze zmenit)}
pp:=np;
end;

Procedure TEdRadek.Zamena(s:pchar);
begin
Vyjmi(1,delka);
Vloz(s,1);
end;

Procedure TEdRadek.ZamenaS(s:string);
begin
s:=s+#0;
Zamena(@s[1]);
end;

Procedure TEdRadek.Rozdel(var nv:TEdRadek;pozice,vypust:longint);
var n:pchar;
    i:longint;
begin
n:=p;
dec(pozice);
inc(n,pozice+vypust);
i:=spp-pozice-1;
nv.Vloz(n,i-vypust,1);
Vyjmi(pozice+1,i);
end;

Procedure TEdRadek.Rozdel(var nv:TEdRadek;pozice:longint);
begin
Rozdel(nv,pozice,0);
end;

Function TEdRadek.ZacatekSlova(poz:longint):longint;
begin
while (znak(poz)<>32{mezera}) do
   if poz=1 then Exit(0) else dec(poz);
ZacatekSlova:=poz;
end;

Function TEdRadek.KonecSlova(poz:longint):longint;
var r:longint;
begin
r:=delka;
while (znak(poz)<>32{mezera}) and (poz<r) do inc(poz);
KonecSlova:=poz;
end;

Procedure TEdRadek.Slovo(poz:longint;var zac,kon:longint);
begin
zac:=ZacatekSlova(poz);
kon:=KonecSlova(poz);
end;

Destructor TEdRadek.Done;
begin
if p<>nil then FreeMem(p);
pp:=0;
spp:=0;
end;


Constructor TItRadek.Init;
begin
inherited Init;
PrvotniPrirazeni;
end;

Constructor TItRadek.Init(s:string);
begin
inherited Init;
PrvotniPrirazeni;
PrvniUzel;
VlozS(s,1);
end;

Procedure TItRadek.PrvotniPrirazeni;
begin
crlf:=true;
up:=0;
gd:=0;
aa:=nil;
so:=-1;
su:=-1;
bu:=0;
odz:=0;
doz:=0;
posunm:=0;
posunv:=0;
end;

Function TItRadek.Delka:longint;
begin
Delka:=up;
end;

Function TItRadek.Vyska:longint;
begin
Vyska:=so+su;
end;


Function TItRadek.Znak(n:longint):word;
var t:pchar;
begin
n:=n*2-2;
if n<0 then Znak:=0 else
if n>spp-1 then Znak:=0 else
   Znak:=word(p[n])+word(p[n+1])*256;
end;


Procedure TItRadek.InternalVratCast(var poz,delk:longint;buffer:pchar);
var i:longint;
begin
dec(poz);
if poz<0 then poz:=0;
if poz>=up then poz:=up-1;
if poz+delk-1>up then delk:=up-poz+1;
for i:=0 to delk-1 do buffer[i]:=p[(poz+i)*2];
end;


Function TItRadek.Najdi(s:string;poz:longint):longint;
var a,b,c:longint;
    ne:boolean;
begin
if (poz<1) or (poz>up) then Exit(0);
dec(poz);
b:=Length(s);
for a:=poz to delka-b do
   begin
   ne:=false;
   for c:=0 to b-1 do
      if p[(a+c)*2]<>s[c+1] then begin ne:=true;Break;end;
   if NE=false then Exit(a+1);
   end;
Najdi:=0;
end;


Procedure SmazUzel(var p:pointer);
var f:PfnAtrb;
begin
f:=p;
Dispose(f,Done);
p:=nil;
end;


Function TItRadek.ZrusUzly(poz,l:longint):boolean;
var zmenauzlu:boolean;
    s,q,r:PUzel;
    v:PFNatrb;

begin
s:=VratVaznikUzlu(poz);
if s=nil then q:=nil else q:=s^.dalsi; {1.uzel necha byt}
zmenauzlu:=false;
while q<>nil do
   begin
   v:=q^.vazba;
   r:=q^.dalsi;
   if v^.pozice<poz+l then {kdyz je uvnitr mazaneho useku uzel,}
      begin                {tak ho musim zrusit}
      Dispose(v,Done);
      aa^.ZrusUzel(q);
      zmenauzlu:=true;
      end
      else dec(v^.pozice,l); {kdyz je uzel az za usekem, tak jenom upravim jeho polohu}
   q:=r;
   end;
end;


Procedure TItRadek.Vyjmi(poz,l:longint);
var i,_p_:longint;
    zmenauzlu:boolean;
begin
zmenauzlu:=ZrusUzly(poz,l);
i:=SirkaUseku_IT(@self,poz,l);
inherited vyjmi(poz*2-1,l*2);     {predek nepouziva wordy, ale bajty}
if zmenauzlu then                 {odmazal jsem nejake uzly?}
   begin                          {v tom pripade prepocitam vysku radky}
   so:=-1;                        {"resetuju" aktualni stav}
   VyskaRadky_IT(@self,_p_,so,su);{a vypocitam novy stav}
   end;
dec(gd,i);
up:=(spp-1) div 2;
Obnov_BU;
end;


Procedure TItRadek.VyjmiPosl;
{smaze posledni znak}
begin
Vyjmi(up,1);
end;


Procedure TItRadek.Smaz;
begin
Vyjmi(1,up);
end;


Procedure TItRadek.VlozKus(s:pchar;poz:longint;s_zac,s_del:longint);
var c:char;
begin
dec(s_zac);
inc(s,s_zac);
c:=s[s_del];
s[s_del]:=#0;
Vloz(s,s_del,poz);
s[s_del]:=c;
end;


Procedure TItRadek.VlozWS(t:pchar;td,poz:longint;b:boolean);
{zapisuje PChar z Widecharu}
var i,l:longint;
    q:PUzel;
    v:PFNatrb;
begin
l:=poz*2-1;
q:=VratVaznikUzlu(poz);
inherited vloz(t,td*2,l);
if b then
   begin
   i:=SirkaUseku_IT(@self,q,poz,td);
   inc(gd,i);
   end;
if q<>nil then q:=q^.dalsi;
while q<>nil do
   begin
   v:=q^.vazba;
   inc(v^.pozice,td);     {musim upravit polohu vsech uzlu, ktere budou lezet}
   q:=q^.dalsi;           {za vlozenym usekem}
   end;
up:=(spp-1) div 2;
end;

Procedure TItRadek.VlozWord(o:word;poz:longint);
var a:array[1..3] of byte;
begin
a[1]:=Lo(o);
a[2]:=Hi(o);
a[3]:=0;
VlozWS(@a,1,poz,true);
end;


Procedure TItRadek.Vloz(s:pchar;sd,poz:longint);
var i,k,m,o,r:longint;
    b:boolean;
    j:word;
    n:byte;
    v:PFNatrb;
    t:pchar;
begin
if sd=0 then Exit;
v:=VratUzel(poz);
if v=nil then b:=false else b:=PFont(v^.font)^.unicode;
if B=false then
   begin
   {jestlize neni nastaven unicode font, tak predpokladej, ze jde o ANSI text}
   k:=sd*2+1;
   GetMem(t,k);
   for i:=0 to sd-1 do
       begin
       j:=i*2;
       t[j]:=s[i];    {mene vyznamny bajt prvni, vyznamnejsi druhy}
       t[j+1]:=#0;    {(Inteli syntaxe)}
       end;
   end
   else begin
   {jestli piseme unicode fontem, tak predpokladej, ze text je v UTF-8}
   m:=UniLength(s);   {kolik retezec vlastne obsahuje znaku?}
   k:=m*2+1;
   GetMem(t,k);
   i:=1;
   for o:=0 to {m-2}m-1 do
       begin
       j:=UTF82word(s,sd,i,n);
       r:=o*2;
       t[r]:=char(Lo(j));
       t[r+1]:=char(Hi(j));
       inc(i,n);
       end;
   sd:=m;
   end;
t[k-1]:=#0;
VlozWS(t,sd,poz,true);  {T je S rozepsane na WideChary}
FreeMem(t,k);
end;


Procedure TItRadek.UtvorZakladniUzel;
var v:PFNatrb;
begin
v:=New(PFNatrb,Init);
aa:=NovyVaznik;
aa^.InitNext(v);
end;

Function PFNshoda(a,b:PFNatrb):boolean;
var i:boolean;
begin
PFNshoda:=((a^.font=b^.font) and (a^.pozadi=b^.pozadi) and (a^.extra=b^.extra) and
           (a^.podtrh=b^.podtrh) and (a^.barva=b^.barva));
end;

Procedure UpravPozice(p:PVaznik;poz:longint);
var e:PFnAtrb;
begin
p^.Reset;
while not p^.Konec do
   begin
   e:=p^.Nacti;
   inc(e^.pozice,poz-1);
   end;
end;

Procedure TItRadek.VlozKusIT(s:PItRadek;poz,s_zac,s_del:longint);
var c:char;
    m,n:PChar;
    p1,p2,q2:PUzel;
    v1,v2,v:PFNatrb;
    retez:PVaznik;
    mame_uzly,zmena:boolean;

    i,s_kon,cv,hv,dv:longint;
begin
i:=up+1;
if poz>i then poz:=i;
if s_zac>s^.up then s_zac:=s^.up;
if s_zac+s_del-1>s^.up then s_del:=s^.up-s_zac+1;

mame_uzly:=aa<>nil; {kdyz prijimajici nema zalozene uzly, tak s uzly vubec}
                    {nebudeme pracovat}

retez:=s^.ZkopirujUsekVaznikuUzlu(s_zac,s_del);
if retez=nil then p2:=nil else p2:=retez^.first;
                              {zjisti aktualni uzel pro zacatek vkladaneho}
                              {kdyz nema zadne uzly, tak P2 je NIL}
                              {to nevadi}

inc(gd,SirkaUseku_IT(s,p2,s_zac,s_del)); {pripocti sirku vkladaneho textu}

if mame_uzly then   {jestlize prijimaci ma uzly, tak zpracujeme i uzly}
   begin            {vkladaneho}
   if retez<>nil then    {a vkladany uzly ma? Jestli ne, tak taky nic}
      begin              {delat nebudeme}
      VyskaUsekuRadky_IT(s,s_zac,s_del,cv,hv,dv);
      if hv>so then so:=hv;
      if dv>su then su:=dv;

      UpravPozice(retez,poz);
      p1:=VratVaznikUzlu(poz);   {jaky je uzel prijimajiciho v pozici POZ?}
      v1:=p1^.vazba;


      v2:=p2^.vazba;

      if poz=up+1 then   {kdyz vkladame na konec tak za vlozenym usekem uz nebudou}
         begin           {zadne uzly puvodniho}
         if v1^.pozice=poz then     {primo na vkladanem miste je uzel?}
            begin {muze jit o predpripraveny uzel prazdneho retezce nebo o}
                  {uzel nastaveny az jedno misto za posledni znak}
            Dispose(v1,Done); {Takovy uzel kazdopadne smaz}
            aa^.ZrusUzel(p1); {i odkaz na nej}
            end;
         aa^.AbsorbujVaznik(aa^.last,retez);  {a na konec pripoj vkladany}
         end;
      end;
   end;

Obnov_BU;

m:=s^.p;
inc(m,(s_zac-1)*2);         {pozice, od ktere vkladat}
inherited vloz(m,s_del*2,poz*2-1);
up:=(spp-1) div 2;
end;

Procedure TItRadek.Pripoj(s:PItRadek);
begin
VlozKusIT(s,spp,1,s^.spp-1);
crlf:=s^.crlf;
end;

Function TItRadek.VratVaznikUzlu(poz:longint):PUzel;
var v:PFnatrb;
    q:PUzel;
begin
if aa=nil then Exit(nil);
if poz<2 then Exit(aa^.First);
if poz>=up then Exit(aa^.Last);

q:=aa^.First;
while q<>nil do
   begin
   v:=q^.vazba;
   if v^.pozice>poz then
      begin
      if q<>aa^.first then Exit(q^.predchozi); {podminka by mela platit vzdy}
      Break;
      end;
   q:=q^.dalsi;
   end;
VratVaznikUzlu:=aa^.Last;
end;

Procedure TItRadek.UmistiUzel(u:PFNatrb;poz:longint);
var e:PUzel;
    w:PFNatrb;
    s:Pstring;
    yd:longint;
begin
if poz<1 then poz:=1;         {osetri nesmyslne rozsahy: moc male}
if poz>up+1 then poz:=up+1;   {moc velke}
u^.pozice:=poz;
if aa=nil then           {zadny uzel zatim neexistuje?}
   begin
   poz:=1;               {v tom pripade jsme prvni}
   aa:=NovyVaznik;       {musime kvuli tomu zalozit vaznik}
   aa^.InitNext(u);      {...a priradit}
   end
   else begin
   e:=VratVaznikUzlu(poz);
   w:=e^.vazba;               {nejaky uzel uz tedy existuje...}
   if w^.pozice=poz then      {a nesedi nektery presne na nasi pozici?}
      begin
      if w^.extra<>nil then   {jestli ma nejaky extra tag, tak ho musime}
         begin                {zachovat}
         if u^.extra=nil then     {kdyz novy uzel zadny extraatribut nema,}
            begin
            u^.extra:=w^.extra;   {tak si ho pretahnu k some}
            w^.extra:=nil;        {a v puvodnim uzlu ho zrusim}
            end
            else begin        {novy uzel ma rovnez jakysi extraatribut}
            s:=NaPstring(w^.extra^+';'+u^.extra^); {tak ho spoj dohromady}
            ZrusPstring(u^.extra); {s uzlem puvodniho uzlu}
            u^.extra:=s;
            end;
         end;
      Dispose(w,Done);        {v tom pripade ten stavajici zrusim}
      e^.vazba:=u;            {a namisto nej soupnu novy}
      end
      else begin         {primo na nasi pozici zadny uzel nesedi...}
      aa^.InsertNew(e,u);{tak zaloz novy uzel s pozadovanymi vlastnostmi}
      end;
   end;

if PFont(u^.font)^.so>so then so:=PFont(u^.font)^.so;
if PFont(u^.font)^.su>su then su:=PFont(u^.font)^.su;

if u^.extra<>nil then
   begin
   yd:=NajdiKritickyObrazek(u^.extra^);
   if yd>bu then bu:=yd;
   end;

u^.pozice:=poz;
end;


Procedure TItRadek.Obnov_BU;
var v:PFNAtrb;
    a,b:longint;
begin
if aa=nil then Exit;
a:=0;
aa^.Reset;
while not aa^.Konec do
   begin
   v:=aa^.Nacti;
   if v^.extra<>nil then
      begin
      b:=NajdiKritickyObrazek(v^.extra^);
      if b>a then a:=b;
      end;
   end;
bu:=a;
end;


Procedure TItRadek.PrvniUzel;
var v:PFNAtrb;
begin
v:=New(Pfnatrb,Init);
v^.Default;
UmistiUzel(v,1);
end;


Function TItRadek.VratUzel(poz:longint):PfnAtrb;
var q:PUzel;
begin
q:=VratVaznikUzlu(poz);
if q=nil then VratUzel:=nil else VratUzel:=q^.vazba;
end;


Function TItRadek.ZkopirujUsekVaznikuUzlu(poz,del:longint):PVaznik;
var u:PVaznik;
    q:PUzel;
    poz2:longint;
    d,e:PFnatrb;
begin
if aa=nil then Exit(nil);
poz2:=poz+del-1;
u:=NovyVaznik;
q:=VratVaznikUzlu(poz);
while q<>nil do
   begin
   d:=q^.vazba;
   if d^.pozice<=poz2 then
      begin
      if d^.pozice<poz then
         begin
         e:=d^.CopyTo(1);
         end
         else begin
         e:=d^.CopyEx;
         e^.pozice:=e^.pozice-poz+1;
         end;
      u^.InitNext(e);
      end else Break;
   q:=q^.dalsi;
   end;
ZkopirujUsekVaznikuUzlu:=u;
end;

Function TItRadek.Copy:PItRadek;
begin
Copy:=VratUsek(1,up);
end;

Function TItRadek.VratUsek(poz,del:longint):PItRadek;
var v:PItRadek;
    cv,hv,dv:longint;
    r:PVaznik;
    q:pchar;
begin
if poz>up then Exit(nil);
if del<1 then Exit(nil);
if poz+del-1>up then del:=up-poz+1;

v:=New(PItRadek,Init);
q:=p;
inc(q,(poz-1)*2);
v^.VlozWS(q,del,1,true);
r:=ZkopirujUsekVaznikuUzlu(poz,del);
v^.aa:=r;
if r<>nil then
   begin
   VyskaRadky_IT(v,cv,hv,dv);
   v^.so:=hv;
   v^.su:=dv;
   end;
VratUsek:=v;
end;

Procedure TItRadek.Rozdel(var nv:TItRadek;pozice,vypust:longint);
begin
nv.VlozKusIT(@self,1,pozice+vypust,up-pozice-vypust+1);
Ochrana_obrazku:=true;      {Opatreni, aby TfnAtrb.Done nevymazal z pameti}
Vyjmi(pozice,up-pozice+1);  {obrazek. My ho totiz nechceme smazat, ale jen}
Ochrana_obrazku:=false;     {presunout do jineho TItRadku.}
end;

Procedure TItRadek.Rozdel(var nv:TItRadek;pozice:longint);
begin
Rozdel(nv,pozice,0);
end;

Destructor TItRadek.Done;
begin
if aa<>nil then Vaznik_Done_all(aa,@Kill_pfnatrb);
inherited Done;
end;

end.
