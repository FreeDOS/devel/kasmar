unit filesele;
{��������������������������������� Filesele ���������������������������������}
{
  Tato jednotka je soucasti baliku Wokna32 od Laaci.
  Zde jsou deklarace objektu pouzitelnych pri dialogu ohledne vyberu nebo
  ulozeni souboru. Predpokladam, ze v hlavnim programu tyto funkce nebudou
  obvykle volany primo, ale skrze vysokourovnove procedury z jednotky
  Nadwokna
  ---------
}
{����������������������������������������������������������������������������}
{$INCLUDE defines.inc}
{Zde jsou definovany fileselectory}

interface
uses Wokna32,FNfont2,Vaznik,TEdRadky;

type
PSouborListbox = ^TSouborListbox;
TSouborListbox = object(TListbox)
      zvlastni_koncovky:string;
      ttxtnormalbarva:word;
      ttxtextra1barva:word;
      ttxtextra2barva:word;
      cesta:pstring;
      Constructor Init(ix,iy,isirka,ivyska:longint;p:Pvaznik;icesta:pointer;_multi,iakt:boolean;izk:string);
      Function NatahniData(p:pointer):PItRadek;virtual;
      Destructor Done;virtual;
      end;

PAdrListbox = ^TAdrListbox;
TAdrListbox = object(TListbox)
      ttxtnormalbarva:word;
      cesta:pstring;
      Constructor Init(ix,iy,isirka,ivyska:longint;p:Pvaznik;icesta:pointer;_multi,iakt:boolean);
      Function NatahniData(p:pointer):PItRadek;virtual;
      Destructor Done;
      end;


PDiskListBox = ^TDiskListBox;
TDiskListbox = object(TListBox)
      Constructor init(ix,iy,isirka,ivyska:longint;p:Pvaznik;_multi,iakt:boolean;ivyznam:longint);
      Procedure Ukaz_informaci_o_disku;virtual;
      Destructor Done;virtual;
      end;


PDiskOkno = ^TDiskOkno;
TDiskOkno = object({TLBokno}TOkno_s_tlacitky)
      lb:PDiskListBox;
      Constructor Init(ix,iy,ivyska:longint;titulek:string;tlacitka:string;p:PVaznik;multi:boolean);
      Procedure Akce(var i:longint);virtual;
      Destructor Done;virtual;
      end;


PFileSelector = ^TFileSelector;
TFileSelector = object(TOkno_s_tlacitky)
      koncovky,maska:string;
      moznosti:byte;
      lbs:PSouborListbox;
      lba:PAdrListbox;
      tpl:PChytreTextovePole;
      seznam_souboru,vystup,seznam_adresaru:PVaznik;
      mojedisky,ret:string;
      pocetdisku:byte;
      cesta:array[1..20] of string;
      puv_maska:string;
      csd:byte;
      Constructor Init(ix,iy,isirka,ivyska:longint;titulek,imaska,ikoncovky:string;imoznosti:byte);
      Procedure Kontrola;virtual;
      Procedure PrvniZpracovaniKlaves;virtual;
      Procedure Run;virtual;
      Procedure Akce(var i:longint);virtual;
      Procedure ZmenCestu(p:PUzel);
      Procedure ZmenaAdresare;
      Procedure ZmenaDisku;
      Procedure VytvorAdresar;
      Function RucniVstup:boolean;
      Procedure NatahniListboxy;virtual;
      Procedure NapisInfo;
      Procedure PredkousejKoncovky(s:string);
      Procedure TXT_radek(var i:longint);
      Procedure Vyres_Disky_a_cesty;
      Procedure Over_Zda_muzeme_skoncit(var i:longint);
      Function VratHodnotu_z_LBS:string;
      Function VratHodnotu:string;
      Function VratMultiHodnotu:PVaznik;
      Destructor Done;virtual;
      end;


implementation
uses VenomGFX,Lacrt,Nadwokna,FileList,DOS,W32const,rezklav;

const kod_TDiskLB = -100;


{Constructor TSouborListbox.Init(ix,iy,isirka,ivyska:longint;p:Pvaznik;icesta:pstring;_multi,iakt:boolean;izk:string);}
Constructor TSouborListbox.Init(ix,iy,isirka,ivyska:longint;p:Pvaznik;icesta:pointer;_multi,iakt:boolean;izk:string);
begin
ttxtnormalbarva:=0;
ttxtextra1barva:=21183;
ttxtextra2barva:=64170;
zvlastni_koncovky:=izk;
cesta:=icesta;
inherited Init(ix,iy,isirka,ivyska,p,_multi,iakt,0);
end;

Function TSouborListbox.NatahniData(p:pointer):PItRadek;
var v:Psoubor_szn;
    k,s:string;
    i,jfn:longint;
    c:char;

begin
v:=p;
s:=v^.jmeno^+'.'+v^.koncovka^;
k:=Convert_UP('.'+v^.koncovka^+'|');
i:=Search(zvlastni_koncovky,k,1);

jfn:=FN_color;
FN_color:=ttxtnormalbarva;
if i<>0 then
   begin
   c:=zvlastni_koncovky[i+Length(v^.koncovka^)+2];
   if c='|'
      then s:='<BARVA='+mystr(ttxtextra2barva)+'>'+s+'<SB>'
      else s:='<BARVA='+mystr(ttxtextra1barva)+'>'+s+'<SB>';
   end;
s:=s+#0;
NatahniData:=Tagy_na_vaznik(@s[1],nil);
FN_color:=jfn;
end;

Destructor TSouborListbox.Done;
begin
inherited Done;
end;

Constructor TAdrListbox.Init(ix,iy,isirka,ivyska:longint;p:Pvaznik;icesta:pointer;_multi,iakt:boolean);
begin
ttxtnormalbarva:=0;
cesta:=icesta;
inherited Init(ix,iy,isirka,ivyska,p,_multi,iakt,0);
end;

Function TAdrListbox.NatahniData(p:pointer):PItRadek;
var v:Psoubor_szn;
    s:string;
    jfn:longint;
begin
v:=p;
s:=v^.jmeno^+#0;
jfn:=FN_color;
FN_color:=ttxtnormalbarva;
NatahniData:=Tagy_na_vaznik(@s[1],nil);
FN_color:=jfn;
end;


Destructor TAdrListbox.Done;
begin
inherited Done;
end;


Constructor TDiskListBox.init(ix,iy,isirka,ivyska:longint;p:Pvaznik;_multi,iakt:boolean;ivyznam:longint);
begin
inherited Init(ix,iy,isirka,ivyska,p,_multi,iakt,ivyznam);
end;

Procedure TDiskListBox.Ukaz_Informaci_o_disku;{Akce_P(p:PUzel);}
var w:PString;
    p:PUzel;
    c:char;
    b:byte;
    mx,my:longint;
    i,j:array[1..3] of real;
    s:array[1..2] of string;
    t:string;
    d:TOkno_s_tlacitky;

begin
p:=VratHodnotu;
if p=nil then Exit;
w:=p^.vazba;
c:=w^[1];

if Is_drive_ready(c)=false then OKokno(w_CHYBA,w_FS_JEDNOTKA_NEPRIPRAVENA)
   else begin
   i[1]:=TotaldiskSpace(c);
   i[2]:=FreeDiskSpace(c);
   for b:=1 to 2 do
      if i[b]>1024*1024*1024 {GB} then begin j[b]:=i[b]/(1024*1024*1024);s[b]:=FormNum(j[b],2)+w_FS_GB2;end else
      if i[b]>1024*1024 {MB} then begin j[b]:=i[b]/(1024*1024);s[b]:=FormNum(j[b],2)+w_FS_MB2;end else
      if i[b]>1024 {KB} then begin j[b]:=i[b]/(1024);s[b]:=FormNum(j[b],2)+w_FS_KB2;end else
                          begin j[b]:=i[b];s[b]:=MyStr(round(j[b]))+w_FS_B2;end;
   d.init(NA_STRED,NA_STRED,w_FS_DISK3+c+':',w_OK);
   d.Roztahni(250,250);
   mx:=d.x+3;
   my:=d.y+d.vyskazahlavi;
   d.zobraz;
   t:=DriveName(c);
   if t<>'' then t:=w_FS_JMENOVKADISKU+'<BARVA=65535>'+t+'<SB>'#13#10;
   t:=t+w_FS_SYSTEMSOUBORU+'<BARVA=65535>'+FATtype(c)+'<SB>'#13#10+
        w_FS_SERCISLO+'<BARVA=65535>'+SerioveCisloDisku(c)+'<SB>';

   i[3]:=i[1]-i[2];
   i[2]:=i[2]/i[1]*1000;
   i[3]:=i[3]/i[1]*1000;

   KolacovyGraf(d.x+d.sirka div 2,d.y+d.vyska div 2-4,65,mystr(round(i[3]))+','+mystr(round(i[2])));

   MouseHide;
   _print(mx,my,BA_chy_txt,t);

   my:=d.y+d.vyska div 2+60;
   _print(mx,my,BA_chy_txt,w_FS_KAPACITA+s[1]);
   _print(mx,my+16,kolgrafbarva[2],w_FS_VOLNEHO+s[2]);
   MouseShow;
   d.run;
   d.done;
   end;

end;

Destructor TDiskListBox.Done;
begin
inherited Done;
end;


Constructor TDiskOkno.Init(ix,iy,ivyska:longint;titulek:string;tlacitka:string;p:PVaznik;multi:boolean);
var i:longint;
begin
TOkno_s_tlacitky.init(ix,iy,titulek,tlacitka);
lb:=New(PDiskListBox,Init(0,0,sirka-4,ivyska{-vrsek a spodek},p,multi,true,kod_TDiskLB));
VlozNahoru(lb);
end;


Procedure TDiskOkno.Akce(var i:longint);
begin
if i=kod_TDiskLB then i:=ww_OK;
if i=ww_fs_info then lb^.Ukaz_informaci_o_disku;
end;


Destructor TDiskOkno.Done;
begin
inherited done;
end;


Procedure TFileSelector.PredkousejKoncovky(s:string);
var a,i:byte;
begin
a:=1;
i:=Length(s);
while a<=i do
   begin
   if s[a]='|' then
      begin
      if (a<i) and (s[a+1]<>'|') then
         begin
         insert('.',s,a+1);
         inc(i);
         inc(a);
         end;
      end;
   inc(a);
   end;
insert('.',s,1);
s:=Convert_Up(s)+'|';
koncovky:=s;
end;

Procedure TFileSelector.Vyres_Disky_a_cesty;
var c:char;
    s:string;
    a:byte;
begin
s:=logdisk;
mojedisky:=LogDiskPlus;
pocetdisku:=Length(mojedisky);
for a:=1 to pocetdisku do cesta[a]:=mojedisky[a]+':\';

if not ZkontrolujCestu(maska) then
   begin
   s:=FExpand(puv_maska);
   maska:=StripNameExt(s);
   s:=StripPath(s);
   csd:=Pos(s[1],mojedisky);
   cesta[csd]:=s;
   end
   else begin
   s:=FExpand(maska);
   c:=UpCase(s[1]);
   csd:=Pos(c,mojedisky);
   maska:=StripNameExt(s);
   cesta[csd]:=StripPath(s);
   end;
end;


Constructor TFileSelector.Init(ix,iy,isirka,ivyska:longint;titulek,imaska,ikoncovky:string;imoznosti:byte);
var s:string;
    i:longint;
    v:PTlacitko;

begin
puv_maska:=imaska;
maska:=imaska;
moznosti:=imoznosti;
PredkousejKoncovky(ikoncovky);
if (moznosti and 1)=0
   then
   if (moznosti and 2)=0
      then s:=w_ZADAT+#13#10+w_FS_INFO+#13#10+w_FS_DISK+#13#10+w_OK2+#13#10+w_CANCEL
      else s:=w_ZADAT+#13#10+w_FS_INFO+#13#10+w_FS_DISK+#13#10+w_FS_NOVYADRESAR+#13#10+w_OK2+#13#10+w_CANCEL
   else s:=w_ZADAT+#13#10+w_FS_INFO+#13#10+w_OK2+#13#10+w_CANCEL;

inherited Init(ix,iy,titulek,s);
if isirka<400 then isirka:=400;
Roztahni(isirka,ivyska);

i:=retez^.NejsirsiObjekt;
Roztahni_a_umisti_tlacitka(x+sirka-i-5,y+VyskaZahlavi+1,i,2);

seznam_souboru:=NovyVaznik;
seznam_adresaru:=NovyVaznik;

lbs:=nil;
lba:=nil;
Vyres_disky_a_cesty;
if (moznosti and 1)<>0 then s:=maska else s:=cesta[csd]+maska;
tpl:=New(PChytreTextovePole,Init(0,0,sirka-5,s,false,ww_fs_txt_radek));
tpl^.ZmenPozici(x+3,y+vyska-tpl^.vyska-2);
Retez^.Pridej(tpl);
NatahniListboxy;
end;

Procedure TFileSelector.NatahniListboxy;
var r1,r2:real;
    cx:longint;
    v:PTlacitko;
    multi:boolean;
    s:string;

begin
if lba<>nil then
   begin
   retez^.Odeber(lba);
   ZrusSeznamAdresaru(seznam_adresaru);
   Dispose(lba,Done);
   end;

if lbs<>nil then
   begin
   retez^.Odeber(lbs);
   ZrusSeznamSouboru(seznam_souboru);
   Dispose(lbs,Done);
   end;

NactiSeznamSouboru(seznam_souboru,cesta[csd]+maska);  {seznam}
v:=Retez^.Uzel(1);
cx:=sirka-v^.sirka-5;
multi:=(moznosti and 128)<>0;
if odd(moznosti)=false then
   begin
   r1:=cx*3/9;
   NactiSeznamAdresaru(seznam_adresaru,cesta[csd]+'*.*');
   lba:=New(PAdrListbox,Init(0,0,round(r1),vyska-VyskaZahlavi-tpl^.vyska-3,seznam_adresaru,@cesta[csd],false,false));
   lba^.vyznam:=ww_fs_lba;
   lbs:=New(PSouborListbox,Init(0,0,cx-lba^.sirka-10,vyska-VyskaZahlavi-tpl^.vyska-3,seznam_souboru,@cesta[csd],multi,true,koncovky));
   lbs^.vyznam:=ww_fs_lbs;
   lba^.ZmenPozici(x+3,v^.y);
   lbs^.ZmenPozici(v^.x-lbs^.sirka-5,v^.y);
   Retez^.Pridej(lba);
   Retez^.Pridej(lbs);
   end
   else begin
   lbs:=New(PSouborListbox,Init(0,0,cx-5,vyska-VyskaZahlavi-v^.vyska-3,seznam_souboru,@cesta[csd],multi,true,koncovky));
   lbs^.vyznam:=ww_fs_lbs;
   lbs^.ZmenPozici(x+3,v^.y);
   Retez^.Pridej(lbs);
   end;

if (moznosti and 1)<>0 then s:=maska else s:=cesta[csd]+maska;
tpl^.hodnota^.ZamenaS(s);
tpl^.default:=s;
tpl^.pozice[0]:=1;
tpl^.ZrusBlok;
tpl^.KlavesaEnd;


retez^.Aktivuj(lbs);
end;

Procedure TFileSelector.ZmenCestu(p:PUzel);
var v:Padresar_szn;
    b:byte;
    s:string;
begin
v:=p^.vazba;
s:=v^.jmeno^;
if v^.jmeno^<>'..' then cesta[csd]:=cesta[csd]+s+'\'
   else begin
   b:=BackSearch(cesta[csd],'\',2);
   cesta[csd]:=Copy(cesta[csd],1,Length(cesta[csd])-b+1);
   end;
end;


Procedure TFileSelector.ZmenaAdresare;
var p:PUzel;
begin
p:=lba^.VratHodnotu;
if p<>nil then
   begin
   ZmenCestu(lba^.VratHodnotu);
   NatahniListboxy;
   retez^.Aktivuj(lba);
   Zobraz;
   end;
end;

Procedure TFileSelector.Kontrola;
begin
inherited Kontrola;
end;

Function ZkusZalozitSoubor(s:string):integer;
var f:file;
    j:integer;
begin
j:=filemode;filemode:=2;
Assign(f,s);
Reset(f,1);
ZkusZalozitSoubor:=IOresult;
Close(f);
filemode:=j;
end;

Function TFileSelector.RucniVstup:boolean;
var s,t:string;
    i:integer;
begin
RucniVstup:=false;
s:=tpl^.VratHodnotu;

if (moznosti and 1)<>0 then
   begin
   s:=StripNameExt(s);
   maska:=cesta[csd]+s;
   end
   else maska:=PrechodCesty(cesta[csd],s);

if (Pos('*',maska)=0) and (Pos('?',maska)=0) then
   if (moznosti and 2)<>0 then RucniVstup:=true;
      (*begin
      i:=ZkusZalozitSoubor(maska);
      if i=0 then RucniVstup:=true else OKokno(w_CHYBA,w_FS_SOUBORNEVYTVOREN);
      end else if ExistFile(maska) then RucniVstup:=true;*)


Vyres_disky_a_cesty;
NatahniListboxy;
retez^.Aktivuj(lba);
Zobraz;
end;

Procedure TFileSelector.VytvorAdresar;
var s:string;
    b:byte;
begin
s:=VloztextOkno(w_FS_ADR_JAKSEMAJMENOVAT,500,'');
if s='' then Exit;
MkDir(cesta[csd]+s);
b:=IOresult;
if b<>0 then OKokno(w_chyba,w_FS_ADRESARNEVYTVOREN);
NatahniListboxy;
retez^.Aktivuj(lba);
Zobraz;
end;


Procedure TFileSelector.Over_Zda_muzeme_skoncit(var i:longint);
var t:longint;
    s:string;
begin

t:=tpl^.stav;
s:=tpl^.vrathodnotu;

if lbs^.hotovo=true then Exit; {dvakrat klepnuto do vyberniku souboru?- > OK}
if (moznosti and 2)<>0 then        {jsme v rezimu UlozSoubor?}
   if tpl^.stav=_deaktivace then   {jsme v situaci, ze jsme zrovna odklepli}
                                   {z aktivniho radku?}
      if (Pos('*',s)<>0) or (Pos('?',s)<>0)   {a na tom radku jsou}
         then                                         {zastupne znaky?}
         i:=0 {tak v tom pripade nemuzeme odejit}
         else
         tpl^.hotovo:=true;  {pro pripad, ze se z rozepsaneho textu kleplo}
end;                         {na tlacitko OK}


Procedure TFileSelector.Akce(var i:longint);
begin
case i of
   ww_fs_info:NapisInfo;
   ww_fs_disk:ZmenaDisku;
   ww_fs_zadat:retez^.Aktivuj(tpl);
   ww_fs_vytvoradresar:VytvorAdresar;
   ww_fs_lba:ZmenaAdresare;
   ww_fs_lbs:i:=ww_ano;
   ww_fs_txt_radek:TXT_radek(i);
   ww_ano:Over_Zda_muzeme_skoncit(i);
end;{case}
end;

Procedure TFileSelector.TXT_radek(var i:longint);
begin
if RucniVstup then
   begin
   lbs^.hotovo:=true;
   i:=ww_ano;
   end else tpl^.hotovo:=false;
end;

Procedure TFileSelector.PrvniZpracovaniKlaves;
var i:longint;
begin
if xKlavesa.ASCII=xTab then
   begin
   if tpl^.stav=_aktivni then retez^.Aktivuj_s_vyznamem(ww_fs_lbs) else
   if lbs^.stav=_aktivni then
      if lba<>nil then retez^.Aktivuj_s_vyznamem(ww_fs_lba) else else
   if (lba<>nil) and (lba^.stav=_aktivni) then retez^.Aktivuj_s_vyznamem(ww_fs_lbs) else
      retez^.Aktivuj_s_vyznamem(ww_fs_lbs);
   Zobraz;
   end;
end;

Procedure TFileSelector.Run;
begin
inherited Run;
end;


Function TFileSelector.VratHodnotu_z_LBS:string;
var p:PUzel;
    v:PSoubor_szn;
    s:string;
begin
p:=lbs^.VratHodnotu;
if p<>nil then
   begin
   v:=lbs^.VratHodnotu^.vazba;
   s:=cesta[csd]+v^.jmeno^+'.'+v^.koncovka^;
   VratHodnotu_z_LBS:=s;
   end
   else VratHodnotu_z_LBS:='';
end;


Function TFileSelector.VratHodnotu:string;
begin
if hodnota=ww_zrus then Exit('');
if (moznosti and 2<>0) and (tpl^.hotovo=true) then Exit(tpl^.VratHodnotu);
VratHodnotu:=VratHodnotu_z_LBS;
end;


Function TFileSelector.VratMultiHodnotu:PVaznik;
var q,r:PVaznik;
    p:PUzel;
    e:PSoubor_szn;
    s:string;
begin
if lbs^.hotovo=false then Exit(nil);
q:=lbs^.VratMultiHodnotu;
if q<>nil then
   begin
   r:=NovyVaznik;
   q^.Reset;
   while not q^.Konec do
      begin
      e:=q^.Nacti;
      s:=cesta[csd]+e^.jmeno^+'.'+e^.koncovka^;
      r^.InitNext(NaPstring(s));
      end;
   Exit(r);
   end
   else Exit(nil);
end;

Procedure TFileSelector.NapisInfo;
var dt:datetime;
    atr:string[45];
    soubor:PSoubor_szn;
    p:PUzel;
Begin
p:=lbs^.VratHodnotu;
if p=nil then begin infokno(mouse.x,mouse.y,w_FS_VYBER_NECO);Exit;end;
soubor:=p^.vazba;
with soubor^ do
  begin
  atr:='';            {slozeni popisu atributu:}
  if atributy and ReadOnly=readonly then atr:=w_FS_PRO_CTENI;
  if atributy and Hidden=hidden then atr:=atr+w_FS_SKRYTY;
  if atributy and SysFile=sysfile then atr:=atr+w_FS_SYSTEMOVY;
  if atributy and Archive=archive then atr:=atr+w_FS_ARCHIVOVAT;
  if atr='' then atr:=w_FS_ZADNE
            else begin
                 dec(byte(atr[0]));{odmazani mezery na konci}
                 atr[byte(atr[0])]:='.';{misto carky na konec tecku}
                 end;
  unpacktime(soubor^.zmeneno,dt);
  okokno(w_FS_ZPRAVA,w_FS_SOUBOR+jmeno^+'.'+koncovka^+#13#10+
         w_FS_VELIKOST+{mystr}FormNum(velikost,0)+w_FS_B+{mystr}FormNum(velikost shr 10,0)+w_FS_KB+#13#10+
         w_FS_POSL_ZMENA+mystr(dt.day)+'.'+mystr(dt.month)+'.'+mystr(dt.year)
            +', '+mystr(dt.hour)+':'+mystr3(dt.min,2)+':'+mystr3(dt.sec,2)+#13#10+
         w_FS_ATRIBUTY+atr);
  end;
retez^.Aktivuj(lbs);
Zobraz;
End;

Procedure TFileSelector.ZmenaDisku;
var  p:PVaznik;
     t:TDiskOkno;
     u:PUzel;
     m:longint;
     s:string;
     a:byte;
begin
p:=New(PVaznik,Init);
for a:=1 to pocetdisku do
    begin
    if mojedisky[a] in ['A','B'] then s:='' else s:=DriveName(mojedisky[a]);
    {u disketovych mechanik se nesnaz hned cist nazev disku, zdrzovalo by to}
    if s<>'' then
       s:='  ['+s+']';
    s:=mojedisky[a]+s;
    p^.InitNext(NaPstring(s));
    end;

m:=VyskaPStringu(p);

t.Init(NA_STRED,NA_STRED,m,w_FS_DISK2,w_ok+#13#10+w_FS_INFO+#13#10+w_CANCEL,p,false);
t.lb^.sirka:=t.sirka-6;
t.Zobraz;
t.Run;
if t.hodnota=ww_ok then u:=t.lb^.VratHodnotu else u:=nil;

if u<>nil then csd:=p^.Kolikaty_ve_vazniku(u);
SmazVaznikPStringu(p);
t.Done;
if u<>nil then NatahniListboxy;
retez^.Aktivuj(lba);
Zobraz;
end;

Destructor TFileSelector.Done;
begin
inherited Done;
ZrusSeznamAdresaru(seznam_adresaru);
ZrusSeznamSouboru(seznam_souboru);
end;


end.
