uses VenomGfx,wokna32,fnfont2,lacrt,Vaznik;
(*{$INCLUDE defines.inc}*)
var s:string;
    v:virtualwindow;
    t:tlacitko;
    v1,v2,v3:tlacitko2;
    konec:tlacitko;
    nove:tlacitko;
    d:textovepole;

begin
ObvyklyStart;

FN_PCX_adresar:='.\obrazky\';                {Adresar s obrazky}


Clr(vga,MyRGB2word(0,80,90));

Load_PCX('.\obrazky\vrag.pcx',v);   {Nactu obrazek}
PutSprite(vga,v,0,0);                {A zobrazim ho}

s:=VyberSouborOkno('Vyber soubor:','*.*','',250,0);  {vyska okna bude 250 pixelu}
if s='' then s:='Nic sis nevybral';

t.init(vga.breitediv2,vga.hoehediv2,s,'Pravej geroj n�pov�du nepot�ebuje.',0,1);
{levy horni roh tlacitka ve stredu obrazovky}

{kousek pod nej}
repeat
{mousepoll;}                     {stara se o zobrazovani mysi}
t.kontrola;
until t.klik<>0;

{tlacitko zamazu, uz nebude potreba}
MouseHide;
Bar(vga,t.x,t.y,t.x+t.sirka,t.y+t.vyska,MyRGB2word(0,80,90));
MouseShow;

NastavFont('cour18.FN');        {nastavime jiny font}
nove.init(10,400,'font "cour18.FN" - <FONT=__VGA8>klikni na m�<SF>','K dispozici jsou ASCII fonty pro Latin2 a pro DOSovou azbuku a potom jeden unicode font',0,1);
repeat
{mousepoll;}
nove.kontrola;
until nove.klik<>0;

NastavFont(FN_FONT_VGA16);     {Takhle by se vratim vychozi font}

{tlacitko zamazu, uz nebude potreba}
MouseHide;
Bar(vga,nove.x,nove.y,nove.x+nove.sirka,nove.y+nove.vyska,MyRGB2word(0,80,90));
MouseShow;

v1.init(vga.breitediv2,vga.hoehediv2+40,'Praha','vlajky.kkh:1','vlajky.kkh:2');
v2.init(vga.breitediv2,vga.hoehediv2+90,'Var�ava','vlajky.kkh:3','vlajky.kkh:4');
v3.init(vga.breitediv2,vga.hoehediv2+140,'Pa���','vlajky.kkh:5','vlajky.kkh:6');
konec.init(vga.breitediv2,vga.hoehediv2+190,
          'Klikej na vlaje�ky. Pro ukon�en� klikni sem','',365,2);
repeat
v1.kontrola;
v2.kontrola;
v3.kontrola;
konec.kontrola;
until konec.klik<>0;

{----------------------------------------------------------------------------}
VyberBarvuOkno('Vyber barvu z palety',15);
VyberHiColorBarvuOkno('Vyber HiColor barvu',15);
{----------------------------------------------------------------------------}
{ A JDE SE PRASIT! }


OkOkno('<FONT=surea23.fn>Zm�na <FONT=arial18.fn; BARVA=60000>f<SB>o<BARVA=60000>n<SB>t<BARVA=60000>u<SF> za letu'
      ,'<FONT=ball19.fn>Ahoj - <FONT=rusky16.ch>�ਢ��!<SF>'#13#10'1'#13#10'2'#13#10'3'#13#10'4'#13#10'5 <IMG=malgo.pcx:10,-70>'#13#10'6'#13#10'7<SF;SF>');

kill_mouse;                    {odpojim mys}
Kill_graph;                    {zavru graficky rezim}
end.
