unit barvy;
{���������������������������������� Barvy �����������������������������������}
{
  Tato jednotka je soucasti baliku Wokna32 od Laaci.
  Zde jsou deklarace objektu pouzitelnych pri vyberu amichani barev. Predpo-
  kladam, ze v hlavnim programu tyto funkce nebudou obvykle volany primo, ale
  skrze vysokourovnove procedury z jednotky Nadwokna
                                            ---------
}
{����������������������������������������������������������������������������}
{$INCLUDE defines.inc}

interface
uses Wokna32,VenomGFX;

type

PDuha = ^TDuha;
TDuha = object(TWoknaZaklad)
      duhaimg:PVirtualwindow;
      Constructor Init(ix,iy:longint);
      Procedure Zobraz;virtual;
      Destructor Done;virtual;
      end;

PVyberBarvuOkno =^TVyberBarvuOkno;
TVyberBarvuOkno = object(TOkno_s_tlacitky)
      stara,vybrana:byte;
      lhx,lhy:longint;
      def:byte;
      Constructor Init(ix,iy:longint;titulek:string;default:byte);
      Function VratHodnotu:word;
      Procedure Kontrola;virtual;
      Procedure ZmenPozici(ix,iy:longint);virtual;
      Procedure Zobraz;virtual;
      Procedure ZobrazZadane;
      Destructor Done;virtual;
      end;

PVyberBarvu16Okno = ^TVyberBarvu16Okno;
TVyberBarvu16Okno = object(TOkno_s_tlacitky)
      def,vybrana:word;
      lhx,lhy:longint;
      duha:PDuha;
      Constructor Init(ix,iy:longint;titulek:string;default:word);
      Function VratHodnotu:word;
      Procedure Kontrola;virtual;
      Procedure VolejPaletu;
      Procedure VolejMixer;
      Procedure ZmenPozici(ix,iy:longint);virtual;
      Procedure Zobraz;virtual;
      Procedure ZobrazZadane;
      Destructor Done;virtual;
      end;


PMixBarvuOkno = ^TMixBarvuOkno;
TMixBarvuOkno = object(TOkno_s_tlacitky)
      lhx,lhy:longint;
      def,vybrana:longint;
      r,g,b:PPosuvnik;
      Constructor Init(ix,iy:longint;titulek:string;default:word);
      Function VratHodnotu:word;
      Procedure Zobraz;virtual;
      Procedure ZobrazZadane;
      Procedure ZmenPozici(ix,iy:longint);virtual;
      Procedure Kontrola;virtual;
      Destructor Done;virtual;
      end;



implementation
uses Rezklav,Lacrt,Vaznik,W32const,TEdRadky,FNfont2;


Constructor TDuha.Init(ix,iy:longint);
var i,j:longint;
begin
inherited Init;
x:=ix;
y:=iy;
sirka:=128+128;
vyska:=128+128;
New(duhaimg);
Init_VW(duhaimg^,sirka,vyska,false);
for i:=0 to 127 do for j:=0 to 127 do PutPixel(duhaimg^,127-i,127-j,MyRGB2word(i*2,j*2,0));
for i:=0 to 127 do for j:=0 to 127 do PutPixel(duhaimg^,i+128,127-j,MyRGB2word(i+j,i+j,i+j));
for i:=0 to 127 do for j:=0 to 127 do PutPixel(duhaimg^,127-i,j+128,MyRGB2word(i*2,0,j*2));
for i:=0 to 127 do for j:=0 to 127 do PutPixel(duhaimg^,i+128,j+128,MyRGB2word(0,i*2,j*2));
end;


Procedure TDuha.Zobraz;
begin
MouseHide;
PutClippedSprite(cil^,duhaimg^,x,y);
MouseShow;
end;


Destructor TDuha.Done;
begin
Kill_VW(duhaimg^);
Dispose(duhaimg);
end;


Constructor TvyberBarvuOkno.Init(ix,iy:longint;titulek:string;default:byte);
var v1,v2:PTlacitko;
begin
inherited Init(ix,iy,titulek,w_ok+#13#10+w_CANCEL);
stara:=default;
vybrana:=default;
def:=default;
v1:=Retez^.Uzel(1);
v2:=Retez^.Uzel(2);
Roztahni(16*10+4,16*10+VyskaZahlavi+v1^.vyska+6);  {roztazeni okna}

v1^.ZmenPozici(x+2,v1^.y);
v2^.ZmenPozici(x+sirka-v2^.sirka-3,v1^.y);
lhx:=x+2;
lhy:=y+VyskaZahlavi;
end;


Procedure TVyberBarvuOkno.ZmenPozici(ix,iy:longint);
begin
inc(lhx,ix-x);
inc(lhy,iy-y);
inherited ZmenPozici(ix,iy);
end;


Procedure TVyberBarvuOkno.Kontrola;
begin
inherited Kontrola;
if Retez^.VyznamAktivniho(akt_bez)=ww_Zrus then vybrana:=def;
if Mouse_L and MouseInArea(lhx,lhy,lhx+16*10-1,lhy+16*10-1) then
   begin
   vybrana:=(mouse.x-lhx) div 10+((mouse.y-lhy) div 10)shl 4;
   if vybrana<>stara then
      begin
      ZobrazZadane;
      stara:=vybrana;
      end;
   MouseRel;
   end;
end;

Function TVyberBarvuOkno.VratHodnotu:word;
begin
VratHodnotu:=Vga2Word(vybrana);
end;

Procedure TVyberBarvuOkno.ZobrazZadane;
var s:string;
    v:PItRadek;
    u1,u2:PTlacitko;
begin
MouseHide;
u1:=retez^.Uzel(1);
u2:=retez^.Uzel(2);

Bar(cil^,(stara and 15)*10+lhx,(stara shr 4)*10+lhy,(stara and 15)*10+lhx+9,(stara shr 4)*10+lhy+9,vga2word(stara));
rectangle(cil^,(vybrana and 15)*10+lhx+1,(vybrana shr 4)*10+lhy+1,(vybrana and 15)*10+lhx+9,(vybrana shr 4)*10+lhy+9,0);
rectangle(cil^,(vybrana and 15)*10+lhx,(vybrana shr 4)*10+lhy,(vybrana and 15)*10+lhx+8,(vybrana shr 4)*10+lhy+8,14);

Str(vybrana,s);s:=s+#0;
v:=Tagy_na_vaznik(@s[1],nil);

Bar(cil^,u1^.x+u1^.sirka+1,u1^.y,u2^.x-1,u2^.y+u2^.vyska-1,BA_okn_v);
Print_IT((x+x+sirka) div 2 - (v^.gd div 2),u1^.y+u1^.vyska-4,v);
Dispose(v,Done);
MouseShow;
end;

Procedure TVyberBarvuOkno.Zobraz;
var i,j:longint;
    n:longint;
begin
inherited Zobraz;
MouseHide;
for i:=0 to 15 do
   for j:=0 to 15 do {barvy}
      Bar(cil^,lhx+10*j,lhy+10*i,lhx+10*j+9,lhy+10*i+9,vga2word(i shl 4+j));
MouseShow;
ZobrazZadane;
end;

Destructor TVyberBarvuOkno.Done;
begin
inherited Done;
end;


Constructor TMixBarvuOkno.Init(ix,iy:longint;titulek:string;default:word);
var v1,v2:PTlacitko;
    i,s:longint;
    rh,gh,bh:byte;

begin
inherited Init(ix,iy,titulek,w_ok+#13#10+w_CANCEL);
Roztahni(350,150);
lhx:=x+2;
lhy:=y+VyskaZahlavi;

i:=retez^.NejsirsiObjekt;
Roztahni_a_umisti_tlacitka(x+sirka-i-5,y+VyskaZahlavi+1,i,2);

s:=sirka-4;
vybrana:=default;
def:=default;
word2RGB(vybrana,rh,gh,bh);
r:=New(PPosuvnik,Init(lhx,lhy+60,s,32*s,rh*s,s,poHORZ,0));
r^.BB_pos_v:=64000;
r^.BB_pos_str_v:=58383;
r^.nahoruT.BB_tla_v:=64000;
r^.doluT.BB_tla_v:=64000;

g:=New(PPosuvnik,Init(lhx,lhy+80,s,64*s,gh*s,s,poHORZ,0));
g^.BB_pos_v:=1344;
g^.BB_pos_str_v:=40723;
g^.nahoruT.BB_tla_v:=1344;
g^.doluT.BB_tla_v:=1344;

b:=New(PPosuvnik,Init(lhx,lhy+100,s,32*s,bh*s,s,poHORZ,0));
b^.BB_pos_v:=30;
b^.BB_pos_str_v:=36092;
b^.nahoruT.BB_tla_v:=30;
b^.doluT.BB_tla_v:=30;

Retez^.Pridej(r);
Retez^.Pridej(g);
Retez^.Pridej(b);
end;


Procedure TMixBarvuOkno.ZmenPozici(ix,iy:longint);
begin
inc(lhx,ix-x);
inc(lhy,iy-y);
inherited ZmenPozici(ix,iy);
end;


Procedure TMixBarvuOkno.Kontrola;
begin
inherited Kontrola;
if r^.kopozitiv  or g^.kopozitiv or b^.kopozitiv then
   begin

   ZobrazZadane;
   end;
if Retez^.VyznamAktivniho(akt_bez)=ww_Zrus then vybrana:=def;
end;

Procedure TMixBarvuokno.Zobraz;
begin
inherited Zobraz;
ZobrazZadane;
end;

Procedure TMixBarvuOkno.ZobrazZadane;
var mix:word;
    ff:word;
    rh,gh,bh:byte;
    rr,gg,bb,s:longint;

begin
s:=sirka-4;
rh:=r^.hodnota div s;
gh:=g^.hodnota div s;
bh:=b^.hodnota div s;

mix:=RGB2word(rh,gh,bh);
MouseHide;
Bar(cil^,lhx+71,lhy,lhx+250,lhy+59,BA_okn_v);
Bar(cil^,lhx+1,lhy+5,lhx+69,lhy+39,mix);
Print_FN(lhx+155,lhy+15,Mystr(rh));
Print_FN(lhx+155,lhy+31,Mystr(gh));
Print_FN(lhx+155,lhy+47,Mystr(bh));
ff:=FN_color;
FN_color:=65535;
Rectangle(cil^,lhx+182,lhy+15,lhx+248,lhy+32,0);
Print_FN(x+185,lhy+31,'= '+Mystr(mix),nil);
FN_color:=ff;

rh:=round(rh/31*255);
gh:=round(gh/63*255);
bh:=round(bh/31*255);

Print_FN(lhx+80,lhy+15,Mystr(rh));
Print_FN(lhx+80,lhy+31,Mystr(gh));
Print_FN(lhx+80,lhy+47,Mystr(bh));
Print_FN(lhx+110,lhy+15,'(#'+dec2hex(rh)+')');
Print_FN(lhx+110,lhy+31,'(#'+dec2hex(gh)+')');
Print_FN(lhx+110,lhy+47,'(#'+dec2hex(bh)+')');
MouseShow;
vybrana:=mix;
end;

Function TMixBarvuOkno.VratHodnotu:word;
begin
VratHodnotu:=vybrana;
end;

Destructor TMixBarvuOkno.Done;
begin
inherited Done;
end;

Constructor TVyberBarvu16Okno.Init(ix,iy:longint;titulek:string;default:word);
var v1,v2:PTlacitko;
begin
inherited Init(ix,iy,titulek,w_ok+#13#10+w_CANCEL+#13#10+w_PALETA);
v2:=Retez^.Uzel(2);
v1:=New(PTlacitko,Init(0,0,w_MIXBAREV,0,2,0));
v1^.ZmenPozici(v2^.x,v2^.y-v2^.vyska-5);
Retez^.Pridej(v1);
vybrana:=default;
def:=default;
Roztahni(260,260+vyska+v1^.vyska+1);  {roztazeni okna}
lhx:=x+2;
lhy:=y+VyskaZahlavi;


duha:=New(PDuha,Init(lhx,lhy));
Retez^.Pridej(duha);
end;


Procedure TVyberBarvu16Okno.ZmenPozici(ix,iy:longint);
begin
inc(lhx,ix-x);
inc(lhy,iy-y);
inherited ZmenPozici(ix,iy);
end;


Function TVyberBarvu16Okno.VratHodnotu:word;
begin
VratHodnotu:=vybrana;
end;

Procedure TVyberBarvu16Okno.VolejPaletu;
var t:TVyberBarvuOkno;
begin
t.init(NA_STRED,NA_STRED,w_paleta,15);
t.zobraz;
t.run;
if t.retez^.VyznamAktivniho(akt_bez)<>ww_zrus then vybrana:=t.VratHodnotu;
t.done;
end;

Procedure TVyberBarvu16Okno.VolejMixer;
var t:TMixBarvuOkno;
begin
t.init(NA_STRED,NA_STRED,w_mixbarev,vybrana);
t.zobraz;
t.run;
if t.retez^.VyznamAktivniho(akt_bez)<>ww_zrus then vybrana:=t.VratHodnotu;
t.done;
end;


Procedure TVyberBarvu16Okno.ZobrazZadane;
var v:PTlacitko;
begin
v:=Retez^.Uzel(4);
MouseHide;
Bar(cil^,lhx,v^.y,v^.x-1,v^.y+v^.vyska,BA_okn_v);
Bar(cil^,lhx+1,v^.y+1,lhx+15,v^.y+15,vybrana);
Rectangle(cil^,lhx,v^.y,lhx+16,v^.y+16,0);
Print_FN(lhx+25,v^.y+15,MyStr(vybrana),nil);
MouseShow;
end;

Procedure TVyberBarvu16Okno.Kontrola;
begin
inherited Kontrola;
if Retez^.VyznamAktivniho(akt_bez)=ww_paleta then
   begin
   VolejPaletu;
   ZobrazZadane;
   end;

if Retez^.VyznamAktivniho(akt_bez)=ww_mixbarev then
   begin
   VolejMixer;
   ZobrazZadane;
   end;

if Mouse_L and MouseInArea(lhx,lhy,lhx+255,lhy+255) then
   begin
   MouseHide;
   vybrana:=GetPixel(cil^,mouse.x,mouse.y);
   MouseShow;
   ZobrazZadane;
   MouseRel;
   end;
if Retez^.VyznamAktivniho(akt_bez)=ww_zrus then vybrana:=def;
end;

Procedure TVyberBarvu16Okno.Zobraz;
var i,j:longint;
begin
inherited Zobraz;
ZobrazZadane;
end;

Destructor TVyberBarvu16Okno.Done;
begin
inherited Done;
end;


end.
