  VenomGFX version 8.7, released in 5.1.2019
  -----------------------------------------------------------------------

  VenomGFX is a very fast SVGA 16 Bit graphics library for DOS which can be
  used with Freepascal. It supports both generations - 1.0.10 and 2.x.x
  There are tons of functions and procedures implementated.



  1) OVERVIEW

     - EXTREMELY MANY graphics primitives including some very advanced and
       unique like rotated ellipses, ellipse sectors, chords, boxes with
       rounded corners, polygons, floodfill, etc.
     - most of procedures has variants with and without clipping
     - unique capability for using graphics primitives with thick or even
       thick and masked lines
     - filled graphics primitives can be painted not only with solid color
       but also with texture (four possible variants implemented)
     - huge amount of sprite functions with various effects like transparency,
       lightness, fading, etc...
     - reads most common image formats, include JPGs and PNGs
       Can save BMPs.
     - can load vector image format .WMF
     - Some antialiased procedures: few graphics primitives and resising
       (most of such functions is moved into separate unit Smooth)
     - unique capabilities around refresh rate control including VESA DDC
       interface
     - fast MMX routines
     - extensive mouse support
     - Image loaders can load even files inside .GRP archives.
       (.GRP is archive format used in games Duke Nukem 3D, Blood
       and others created on Build engine). Load them in this way:
       "Load_PCX('images.grp#lion.pcx',w)"

  2) TECHNICAL INFORMATIONS (VESA)

     VenomGFX has only DOS version and supports only 16 bit graphics modes.
     This is of course a limitation but on the other hand it allows to write
     fully optimised routines in assembler. VenomGFX is aimed mostly for games
     and 16-bit modes are a good compromise between speed and quality.

     To use this library your system must support the video standard VESA 1.2
     or higher. In case you have VESA 2.0 (or higher) system you can use also
     fast LFB graphics else banked modes only will be used.
     LFB should be used preferably however all graphics primitives can be
     used also in banked modes.
     The only limitation of bank modes is in some sprite functions. Most of
     them can't be used directly into banked screen so you have to use some
     kind of offscreen buffer in such case.
     However the rendering into offscreen buffer (type VirtualWindow) and
     flipping the result into screen is recomended method in LFB modes too.
     Alternatively you can use two screen pages (variables VGA and VGB) for
     flipping the image content.

     If you have a card properly supporting VESA 3.0 standard you can control
     the monitor refresh frequency. You can even analyse which resolutions and
     refresh rates is your monitor able to display (via VESA/DDC).


  3) MOUSE
     Besides drawing VenomGFX can handle your mouse. If you use the CTmouse
     driver it even works with the mouse wheel. Mouse is watched by handler so
     no polling in your application is required (but is possible).
     VenomGFX displays the mouse cursor in both - LFB and banked modes.


  4) WAYS OF USAGE
     You can use VenomGFX in three ways:
     a) direct drawing on VGA screen - very comfortable but some routines can
        be slow in banked modes. Good choice for "bussines" applications,
        less for games. Note that video memory is slower than normal RAM. And note
        that LFB access might not be available on older cards or under some
        emulators or under Windows.

     b) Draw into some VirtualWindow buffer with same dimensions like your
        video resolution. Drawed frames then copy using the Flip_SVGA procedure
        Good for games and recomened in case when using banked modes.

     c) You can switch on the double buffering using procedure Init_Pagging.
        Then render on VGB buffer and to display it simply call Flip_page.
        All the stuff around changing the working and display pages is driven
        by VenomGFX.
        Alternatively you can also use the "Pagging_mode:=PAG_BUF" option.
        Then it technicaly works like in point B. Supprisingly it is often
        faster than hardware double buffering :-/


  5) ADDONS
     Module VenomGFX itself can load only .BMP files. If you want to load
     more image types you have to load in main program addon units like
     VNM_GIF, VNM_PNG and so on.
     They define functions like Load_GIF or Load_PNG.
     Another addon is the unit Smooth which has procedures for smooth drawing
     and font library FNfont2 and GUI system Wokna32.


  6) CHANGES
     8.7
         - Sometimes called MMX instructions even on systems without MMX
         + TimerTicksFrom  (safe check for timer)


     8.6
         + MoveSprite and MoveSprite_with_bigbuffer functions for
           flicker-free movement of single sprite on screen
         + Can work with .FN fonts using VNM_FN addon unit
         - mouse release was not always properly detected


     8.5
         + New Load_Image function. It is general loader for all graphic
           image files. It detects the file type and calls the proper
           specific loader. For more info look at the end of this document
           at [Note 2]

         + VenomGFX can now use not only VGA fonts from char generator but
           also vector borland .CHR fonts. Loader from .CHR fonts is in
           external file VNM_CHR and can be attached in a modular way
           similarly to image loaders. For more info look at the end of this
           document at [Note 3]

         - fixed bug in Flip_VW procedure which was not able to move
           from VRAM into RAM
         - fixed small bug in PieSlice and PieSlice3D procedures
         + VenomGFX now detects the PCI-vendor and PCI-device codes of SVGA
           Also detects the CPU brand string.
         * WMF loaders tries to be more stable for "strange" WMF files.
         * optimalization of Putchar_FN procedure
         * Procedures Flip_VW and Flip_VW_and_clrscr are now able to copy
           from banked VGA into virtualwindow
         * Mouse handler now record also information about position of last
           right button press and release

     8.4
         + All filled graphics primitives and also procedure Bar, LineHorz and
           LineVert can be painted not only with solid color
           but also with texture (four possible variants implemented)
           For more informations look at procedure SetLineMode

         + Besides texture based filling is possible also XOR painting mode        mode
         * removed unclipped Bar procedures. Now are always clipped.
         + Page switching is now fully available in banked modes too
         * Improved code of most banked procedures


     8.34
         * mouse handler now tries to be bullet-proof and carefully locks
           all touched memory. It requiered to be made many changes in
           banked routines

         * avoids bug in FPC 2.x.x - under some circumstances can't
           properly assemble MEM[] statement (forgets to change segment
           register (from DS to FS))

     8.33
         - Fixed bug in PutSpriteRegion introduced in version 8.3

     8.32
         * GetBest_Fq now returns on widescreen monitors value 60Hz
         * If tries to set into frequency 60Hz does not use the
           custom refresh rate logic

     8.31
         - Fixed bug causing inproper loading of 8x8 VGA font with some
           display drivers
         - Fixed bug in banked mode version of PutHCsprite introduced in 8.3
         - Fixed crash in banked mode occuring in case when mouse interrupt
           was called inside graphics routines
         * Added clipping info into Putchar_FN function and made it compatible
           with Wokna32 v1.x

     8.3
         + Now you can create RLE maps to sprites to significantly (approx 3x)
           speed up PutHCsprite and PutClippedHCsprite procedures
           (works even in banked VGA screens)
         + Mouse cursors now use the RLE acceleration
         * minor fixes in sprite routines
         * banked VGA variants of LineHorz and LineVert are now fully
           optimized
         * PutSprite_and_clear now has clipping and works for banked VGA too
         * PutSpriteRegion now has clipping and works for banked VGA too


     8.23
         - Fixed bug in GetSprite function introduced in version 8.2
         - Fixed annoying bug in ClippedBar function in banked modes
         * basic sprite procedures can be now used in the same way regardless
           if in LFB or banked mode
         - Fixed bug occuring in some circumstances in Clr routine when
           clearing small sprites
         - Fixed potential problem in Kill_Graph procedure


     8.22
         + FastThickLine, ThickLine, LineThickWithMask
         + these graphics primitives: [Rectangle, Polygon, FilledPolygon,
           RotatedNShape and RotatedFilledNShape] have now optional parameters
           for selecting thickness and mask of used lines
         + these grapics primitives: [Circle, FilledCircle, Ellipse,
           FilledEllipse, RotatedChord, RotatedFilledChord, RotatedEllipse and
           RotatedFilledElipse] have now optional parameter Thickness
         * removed procedures Ring and Ring2 because they are now obsolete
         * image loaders are moved into separated units
         + loader for .WMF format
         + even the PasJPEG based JPEG loader can now load images inside
           .GRP archives
         - fixed bug in PNG and GIF loaders resulting to wrong colors
           (this bug was introduced in version 8.2)


     8.21
         * fixed inconsistency with color specification in drawing procedures


     8.2
         + added support for 32-bit BMPs (produced by Microsoft XNA)
         + "Bar" and "ClippedBar" can be drown directly in banked modes
         + Basic test output is now possible without any external units.
           see OutText anf OutTextHigh procedures
         + text can be displayed directly even in banked modes
         * faster videomode setting
         + better CRTC calculation when setting refresh frequency
         * Removed procs SetRequestedFrequency and SetRequestedVideoAccess.
           These parameters can be now specified as optional params in
           Init_Graph function.

         + added procedure Best_mode obtaining the native or "recomended"
           resolution of monitor or the closest available mode to this
         - fixed crash occuring when polygons with extreme many corners was drawn
         + new graphics primitives: RotatedPieSlice, RotatedChord and
           RotatedFilledChord, RotatedNShape, RotatedFilledNShape


     8.12
         - fixed not working mouse handler when compiled with FPC 2.4.0 and
           above

     8.11
         - under windows NT,2000,XP... is the LFB access always disabled now
           because these windows versions don't support it

     8.1
         + can load images inside .GRP archives. (Note that this ability has
           the addon FN_Font2 too)

         - fixed bugs with banked modes for cards with separated read and
           write access windows like ATI Mach64.

         + all graphics primitives can be drown directly on screen even in
           banked modes!

         + graphics primitive PieSlice3D
         - new and better GIF loader. The former one didn't work reliably.
         * new compact JPG loader (however alternatively you still can use
                                   the previous one based on PasJPEG library)
         * optimalizations in many procedures and functions

         + High level procedures can use alternate PutPixel and Line variants.
           In this way you can drive f.e. semitransparent ellipses or XORed
           boxes, etc.

         + improved documentation


     8.0
         first fully official and well documented version


  6) EXAMPLES:
     A)
       uses VenomGFX;
       begin
       Init_Graph(find_mode(800,600));{sets graphics mode}
       Clr(vga,65535);                {fills screen with white color}
       Circle(vga,400,300,80,64000);  {draws red circle on the screen}
       Init_mouse(vga);               {setup mouse driver}
       MouseShow;                     {shows mouse}
       repeat until Mouse_L;          {wait for press of left button}
       while Mouse_L do;              {wait while button not released}
       Kill_graph;                    {done mouse driver and restore textmode}
       end.

     B)
       uses VenomGFX,VNM_png;
       var v:virtualwindow;
       begin
       Init_Graph(find_mode(800,600),LFB_ACCESS,BEST_FQ or 70);
       {sets mode 800x600x16 in LFB mode with highest rate which is your
        monitor able to do or with 70Hz when monitor info not available}

       Load_Image('image.png',v);     {loads image into structore V}
       Circle(v, v.breite div 2, v.hoehe div 2, 50, 10, 64000);
       {into loaded sprite drow 5px thick red circle}
       PutClippedSprite(vga,v,0,0);
       {put this sprite on screen}
       Init_mouse(vga);               {setup mouse driver}
       MouseShow;                     {shows mouse}
       repeat until Mouse_L;          {wait for press of left button}
       while Mouse_L do;              {wait while button not released}
       Kill_graph;                    {done mouse driver and restore textmode}
       end.


  7) CONTACT
     Laaca: Laaca@seznam.cz
            http://www.laaca.borec.cz

     Venom: Venom@venomsoftware.de
            http://www.venomsoftware.de

     (note that Venom already dropped his work on VenomGFX many years ago)


NOTES:




Note 1: (about alternate refresh rate control)
-----------------------------------------------------------------------------
If you want to set different refresh frequency that the default one you can
use two different algorithms how to calculate all necessary parameters for
monitor. As default one is preset the more robust and more accurate routine.
However if you want to minimize code or just from any other reason you can
use the alternate tiny routine. Do it by removing the "_" char from directive
{_$DEFINE SIMPLIER_GTF_CALC}


Note 2: (about image loaders)
-----------------------------------------------------------------------------
It is not more necessary to use specific image loaders like "Load_BMP" or
"Load_JPG" (although it is still possible).
From version 8.41 you can use also general image loader "Load_Image".
Function "Load_Image" detects the image filetype (by extension) and calls
proper specific loader for it.
Natively Load_Image knows only .BMP files. All other files will be
rejected as unknown filetype. To "learn" the Load_Image function to know
other graphic image types just add the desired loader unit in your uses
statement.
Example:        uses VenomGFX;
                var v1,v2:virtualwindow;
                begin
                Init_Graph(find_mode(800,600));       {sets graphics mode}
                if Load_Image('img1.bmp',v1)=0        {tries to load image}
                   then PutClippedSprite(vga,v1,0,0); {and display it}

                if Load_Image('img2.gif',v2)=0          {now with another}
                   then PutClippedSprite(vga,v2,400,0); {image}
                readln;
                Kill_Graph;
                end.

This code is able to display only the IMG1.BMP. Load_Image knows only .BMP
loader. GIF loader is not known so IMG2.GIF file will not be displayed.
To display also IMG2.GIF you have to just add the vnm_GIF into uses statement.

                uses VenomGFX,vnm_GIF;   { <=== HERE }
                var v1,v2:virtualwindow;
                begin
                Init_Graph(find_mode(800,600));       {sets graphics mode}
                if Load_Image('img1.bmp',v1)=0        {tries to load image}
                   then PutClippedSprite(vga,v1,0,0); {and display it}

                if Load_Image('img2.gif',v2)=0          {now with another}
                   then PutClippedSprite(vga,v2,400,0); {image}
                readln;
                Kill_Graph;
                end.

Very simple, isn't it? The order of included units doesn't matter. You can
write both - "uses VenomGFX, vnm_GIF;" and "uses vnm_GIF,VenomGFX;" - both
variants will work.


Note 3: (about fonts)
-----------------------------------------------------------------------------
For serious application you should consider to use not only pure VenomGFX
unit but whole package Wokna32 which contains special font unit FNfont2.
However even pure VenomGFX has some nice and power features about fonts.
It can handle several font formats. Version 8.5 can work with VGA fonts from
char generator and with vector Borland .CHR fonts.
VenomGFX supports dynamic scaling of vector fonts and tries to emulate it also
for VGA or (in future) for other bitmap fonts.
All you need if to
a) set font like: SetFontStyle('vga',16); or SetFontStyle('goth.chr',24);
b) use it: OutText(target,x,y,'some string',color);

You don't have to bother about preloading font files and managing them.
Everything does the VenomGFX do for you.
Unloading is usually not needed for you however you can do it by calling
procedure "Kill_Font(somefont)".

As you can see, you do not have to use any specific font loaders. It works in
the same way as image loading.
VenomGFX.PAS itself can naturaly work only with VGA charset fonts.
But if you simply add the VNM_CHR file into your "USES" statement the unit
"learns" how to work with .CHR fonts.
