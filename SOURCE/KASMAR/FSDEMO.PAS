uses VenomGFX,FNFont2,Lacrt,Wokna32,Nadwokna,disk,Vaznik;

Function VyberSokno_disk(p:pointer):string;
var v:pstring;
begin
v:=p;
VyberSokno_disk:=v^;
end;

var p,q:PVaznik;
    i:longint;
    s:string;
begin
ObvyklyStart;
p:=Fileselector('vyber_soubor','*.*',
      'txt|doc|1st|bmp||jpg||gif||png||pcx|',350,VSNORMAL);
FN_color:=65535;
p^.Reset;
i:=50;
while not p^.Konec do
   begin
   print_fn(10,i,pstring(p^.Nacti)^,fn_vga16);
   inc(i,16);
   end;
Fileselector_smaz(p);

p:=New(PVaznik,Init);
p^.InitNext(NaPstring('<BARVA=480>Karel<SB>'));
p^.InitNext(NaPstring('<FONT=couri18.fn; BARVA=1455>Pavel<SB; SF>'));
p^.InitNext(NaPstring('<BARVA=3409>Mark�ta<SB>'));
p^.InitNext(NaPstring('<BARVA=64455>Ji�ina<SB>'));
p^.InitNext(NaPstring('<FONT=banffn21.fn; BARVA=64000>Svatopluk<SB; SF>'));
p^.InitNext(NaPstring('<BARVA=12>Tade��<SB>'));
p^.InitNext(NaPstring('<BARVA=52033>Vladim�r<SB>'));
p^.InitNext(NaPstring('<BARVA=65009>Veronika<SB>'));
p^.InitNext(NaPstring('<FONT=surea37.fn; BARVA=21654>Josef<SB; SF>'));

q:=DvojListBox('pokus',600,250,@VyberSokno_disk,p,nil);
i:=100;
FN_color:=64000;
if q<>nil then
   begin
   q^.Reset;
   while not q^.Konec do
      begin
      s:=pstring(q^.Nacti)^;
      print_fn(10,i,s,fn_vga16);
      inc(i,RychlaVyskaRadky(s));
      end;
   end;
debug;
{readln;}
ObvyklyKonec;
end.
